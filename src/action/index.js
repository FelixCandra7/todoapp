import {ADD_TODO, TOGGLE_TODO} from './actionTypes'


export const addTodo = ( title, description, year, month, date, hour, minute) => ({
    type: ADD_TODO,
    title,
    description,
    year,
    month,
    date,
    hour,
    minute,
})

export const toggleTodo = (id) => ({
    type: TOGGLE_TODO,
    id,
})


