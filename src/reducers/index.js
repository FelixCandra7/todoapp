import {combineReducers} from 'redux';
import add from './add';
import todos from './todo-list';
import home from './home';

export default combineReducers({
    add,
    todos,
    home,
})