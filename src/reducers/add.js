import * as Action from '../action/actionTypes';
const add = (state=[],action)=>{
    switch(action.type){
        case Action.ADD_TODO:{
            var date='';
            if(action.month.length < 2 && action.date.length < 2){
                date = '0' + action.date + ' / ' + '0' + action.month +
                ' / ' + action.year;
            } else if(action.month.length < 2 && action.date.length == 2){
                date = action.date + ' / ' + '0' +  action.month +
                ' / ' + action.year;
            } else if(action.month.length == 2 && action.date.length < 2){
                date =  '0' + action.date + ' / '  + action.month +
                ' / ' + action.year;
            } else {
                date =  action.date + ' / ' + action.month +
                ' / ' + action.year;
            };

            var time='';
            if(action.hour.length < 2 && action.minute.length < 2){
                time = `0${action.hour} : 0${action.minute}`;
            } else if(action.hour.length < 2 && action.minute.length == 2){
                time = `0${action.hour} : ${action.minute}`;
            } else if(action.hour.length == 2 && action.minute.length < 2){
                time =  `${action.hour} : 0${action.minute}`;
            } else {
                time =  `${action.hour} : ${action.minute}`;
            };

            const newTodo = {
                id: state.length,
                title: action.title,
                description: action.description,
                date: date,
                time: time,
                completed: false,
            };    
        const newState = [...state, newTodo];
        return newState;
        }
        
        default:
        return state;
    }
}

export default add;