import { createStackNavigator, createAppContainer } from "react-navigation";
import HomeScreen from './screen/home/';
import AddScreen from './screen/add/';
const MainStack = createStackNavigator({
    Home: {
      screen: HomeScreen
    },
    Add: {
      screen: AddScreen
    }
  },{
    initialRouteName: "Home",
    headerMode: 'none',
  });

export default TodoApp = createAppContainer(MainStack);