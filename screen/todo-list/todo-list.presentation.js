import React from 'react';
import { View, Switch, Text, StyleSheet, Dimensions } from 'react-native';
import PropTypes from 'prop-types';

const { height } = Dimensions.get('window');

const styles = StyleSheet.create({
    toDoGrid:{
        height: height * 0.15,
        flexDirection: 'column',
        alignItems: 'flex-start',
        paddingLeft: 20,
    },
    itemTitle:{
        fontSize: 20,
        fontWeight: 'bold',
        paddingBottom: 10,
    },
    switchDate:{
        flexDirection: "row",
        alignItems:'flex-start',
    },
});

 class TodoItem extends React.Component {
    onSwitchValueChange = () => {
        this.props.toggleTodo(this.props.data.id);
    }

    render(){
        return(
            <View style={styles.toDoGrid}>            
                <View>
                    <Text style={styles.itemTitle}>{this.props.data.title}</Text>
                        <View style={styles.switchDate}>
                            <Switch 
                                onValueChange={this.onSwitchValueChange}
                                value={this.props.data.completed}>
                            </Switch>
                        <Text>{this.props.data.date} </Text>
                        <Text>{this.props.data.time}</Text>
                        </View>
                    <Text>description: {this.props.data.description}</Text>
                </View>        
            </View>
        );
    }
}

export default TodoItem;

const dataShape = PropTypes.shape({
    id: PropTypes.string,
    title: PropTypes.string,
    description: PropTypes.string,
    year: PropTypes.string,
    month: PropTypes.string,
    date: PropTypes.string,
    hour: PropTypes.string,
    minute: PropTypes.string,
    completed: PropTypes.bool,
      });
      
TodoItem.PropTypes = {
    data: dataShape.isRequired,
    toggleTodo: PropTypes.func.isRequired,
    };
