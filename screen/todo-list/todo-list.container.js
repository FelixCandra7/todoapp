import {connect} from 'react-redux';
import * as Action from '../../src/action/index';
import TodoItem from './todo-list.presentation';

const mapStateToProps = state => ({
    todos: state.todos
});

const mapDispatchToProps = dispatch => ({
    toggleTodo: (id) => {
        dispatch(Action.toggleTodo(id));
    },
})

export default connect(mapStateToProps,mapDispatchToProps)(TodoItem);