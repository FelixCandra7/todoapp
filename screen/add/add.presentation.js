import React  from 'react';
import { View, Text, TouchableOpacity, StyleSheet,
 ScrollView, TextInput, Dimensions  } from 'react-native';
import  DateTimePicker from 'react-native-modal-datetime-picker';
import PropTypes from 'prop-types';

const { height, width } = Dimensions.get('window');

const styles = StyleSheet.create({
    container:{
        flex: 1,
    },
    header:{
        flexDirection: 'row',
        height: height * 0.1,
        backgroundColor: '#f4511e',
        alignSelf: 'stretch',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: width * 0.05,
    },
    headerButton:{
        width: width * 0.15 ,
        height: height * 0.06,
        backgroundColor: '#bf4900',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
    },
    buttonLabel:{
        color: 'white',
        fontWeight: 'bold',
        fontSize: 20,
    },
    form:{
        height: height * 0.9,
        alignSelf: 'stretch',
    },
    titleGrid:{
        flexDirection: 'column',
        alignSelf: 'stretch',
        height: height * 0.17,
        paddingHorizontal: width * 0.04,
        paddingVertical: height * 0.01,
        alignItems: 'flex-start',
        justifyContent: 'space-between',
    },
    itemTitle:{
        color: '#000000',
        fontWeight: 'bold',
        fontSize: 20,
    },
    titleBox:{
        alignSelf: 'stretch',
        height: height * 0.1,
        backgroundColor: "#fcfaf4",
        borderColor: '#c9c9c9',
        borderRadius: 5,
        borderWidth: 1,
        paddingLeft: width * 0.03,
    },
    dateGrid:{
        flexDirection: 'column',
        alignSelf: 'stretch',
        height: height * 0.19,
        paddingHorizontal: width * 0.04,
        paddingVertical: height * 0.01,
        alignItems: 'flex-start',
        justifyContent: 'space-between',
    },
    dateButton:{
        alignSelf: 'stretch',
        height: height * 0.06,
        backgroundColor: '#bf4900',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
    },
    formButtonLabel:{
        color: 'white',
        fontWeight: 'bold',
        fontSize: 15,
    },
    dateTimeLabel:{
        color: '#000000',
        fontWeight: 'bold',
        fontSize: 13,
    },
    descriptionGrid:{
        flexDirection: 'column',
        alignSelf: 'stretch',
        height: height * 0.4,
        paddingHorizontal: width * 0.035,
        paddingVertical: height * 0.01,
        alignItems: 'flex-start',
        justifyContent: 'space-around',
    },
    descriptionBox:{
        alignSelf: 'stretch',
        height: height * 0.3,
        backgroundColor: "#fcfaf4",
        borderColor: '#c9c9c9',
        borderRadius: 5,
        borderWidth: 1,
        paddingLeft: width * 0.03,
    },
    saveButtonGrid:{
        flexDirection: 'column',
        alignSelf: 'stretch',
        height: height * 0.14,
        paddingHorizontal: width * 0.1,
        paddingVertical: height * 0.01,
        alignItems: 'flex-start',
        justifyContent: 'space-between',
    },
    saveButton:{
        alignSelf: 'stretch',
        height: height * 0.06,
        backgroundColor: '#bf4900',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
    }
});

export default class AddScreen extends React.Component{
    state={
        isDatePickerVisible: false,
        title: '',
        description: '',
        year: '',
        month: '',
        date: '',
        hour: '',
        minute: '',
    };

    addTodo = (title,description,year,month,date,hour,minute) => {
        //redux store
        this.props.addTodo(title,description,year,month,date,hour,minute);
        this.setState({
            title: '',
            description: '',
            year: '',
            month: '',
            date: '',
            hour: '',
            minute: '',
        });
        this.props.navigation.navigate('Home');
    };

    handleDatePicked = data => {
        this.setState({
            year: data.getFullYear(),
            month: data.getMonth().toString(),
            date: data.getDate().toString(),
            hour: data.getHours().toString(),
            minute: data.getMinutes().toString()
        });         
        this.hideDatePicker();
    };

    hideDatePicker = () => {
        this.setState({isDatePickerVisible: false});
    };

    showDatePicker = () => {
        this.setState({isDatePickerVisible: true});
    };

    render(){
        return(
            <View style={styles.container}>
                <View style={styles.header}>
                    <TouchableOpacity 
                    style={styles.headerButton}
                    onPress={() => this.props.navigation.navigate('Home')}
                    >
                        <Text style={styles.buttonLabel}>
                            Back
                        </Text>
                    </TouchableOpacity>    
                </View>
                <ScrollView style={styles.form}>
                    <View style={styles.titleGrid}>
                        <Text style={styles.itemTitle}>
                            Title
                        </Text>
                        <TextInput
                            style={styles.titleBox}
                            placeholder="Write the title here"
                            placeholderTextColor="#000000"
                            value={this.state.title}
                            onChangeText={(title) => this.setState({title: title})}
                        />
                    </View>
                    <View style={styles.dateGrid}>
                        <Text style={styles.itemTitle}>
                            When
                        </Text>
                        <TouchableOpacity
                            style={styles.dateButton}
                            onPress={this.showDatePicker}
                        >
                            <Text style={styles.formButtonLabel}>
                                Select Date and Time
                            </Text>
                        </TouchableOpacity>
                        <DateTimePicker
                            mode="datetime"
                            isVisible={this.state.isDatePickerVisible}
                            onConfirm={this.handleDatePicked}
                            onCancel={this.hideDatePicker}
                        /> 
                    </View>
                    <View style={styles.descriptionGrid}>
                        <Text style={styles.itemTitle}>
                            Description
                        </Text>
                        <TextInput
                            style={styles.descriptionBox}
                            placeholder="Write the title here"
                            placeholderTextColor="#000000"
                            multiline
                            textAlignVertical={'top'}
                            value={this.state.description}
                            onChangeText={(description) => this.setState({description: description})}
                        />
                    </View>
                    <View style={styles.saveButtonGrid}>
                        <TouchableOpacity
                            style={styles.saveButton}
                            onPress={() => this.addTodo(
                                this.state.title,
                                this.state.description,
                                this.state.year,
                                this.state.month,
                                this.state.date,
                                this.state.hour,
                                this.state.minute
                            )}
                            >
                            <Text style={styles.formButtonLabel}>
                                Save Item
                            </Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
            </View>
        );
    }
}

AddScreen.PropTypes = {
    addTodo: PropTypes.func.isRequired,
};
