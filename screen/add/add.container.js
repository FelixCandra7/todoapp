import {connect} from 'react-redux';
import * as Action from '../../src/action/index';
import AddScreen from './add.presentation';

const mapStateToProps = state => ({
    add: state.add
});

const mapDispatchToProps = dispatch => ({
    addTodo: (title,description,year,month,date,hour,minute) => {
        dispatch(Action.addTodo(title,description,year,month,date,hour,minute));
    }
})

export default connect(mapStateToProps,mapDispatchToProps)(AddScreen);