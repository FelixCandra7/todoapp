import React  from 'react';
import { View, Text, TouchableOpacity, StyleSheet,
Dimensions, FlatList } from 'react-native';
import TodoList from '../todo-list';
const { height, width } = Dimensions.get('window');
import PropTypes from 'prop-types';


const styles = StyleSheet.create({
    container:{
        flex: 1,
    },
    header:{
        flexDirection: 'row',
        height: height * 0.1,
        backgroundColor: '#f4511e',
        alignSelf: 'stretch',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: width * 0.05,
    },
    title:{
        color: 'white',
        fontWeight: 'bold',
        fontSize: 30,
    },
    headerButton:{
        width: width * 0.15 ,
        height: height * 0.06,
        backgroundColor: '#bf4900',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
    },
    buttonLabel:{
        color: 'white',
        fontWeight: 'bold',
        fontSize: 20,
    },
    itemGrid:{
        height: height * 0.9,
        paddingVertical: height * 0.02,
    }
});

class HomeScreen extends React.Component {
    _renderItem = ({item}) => (
        <TodoList data={item}/>
    );

    render(){
        return(
            <View style={styles.container}>
                <View style={styles.header}>
                    <Text style={styles.title}>To Do List</Text>
                    <TouchableOpacity 
                    style={styles.headerButton}
                    onPress={() => this.props.navigation.navigate('Add')}
                    >
                        <Text style={styles.buttonLabel}>
                            Add
                        </Text>
                    </TouchableOpacity>    
                </View>
                                
                <View style={styles.itemGrid}>
                    <FlatList
                    data={this.props.home}
                    keyExtractor={item=>item.id.toString()}
                    renderItem={this._renderItem}
                    />
                </View>    
                
            </View>
        );
    }
} 

export default HomeScreen;

const dataShape = PropTypes.shape({
    id: PropTypes.string,
    title: PropTypes.string,
    description: PropTypes.string,
    year: PropTypes.string,
    month: PropTypes.string,
    date: PropTypes.string,
    hour: PropTypes.string,
    minute: PropTypes.string,
    completed: PropTypes.bool,
})

HomeScreen.PropTypes = {
    home: PropTypes.arrayOf(dataShape).isRequired,
    addTodo: PropTypes.func.isRequired,
}