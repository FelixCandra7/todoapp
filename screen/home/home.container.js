import HomeScreen from './home.presentation';
import { connect } from 'react-redux';

const mapStateToProps = state => ({
    home: state.home
});

const mapDispatchToProps = dispatch => ({
    addTodo: (title,description,year,month,date,hour,minute) => {
        dispatch(Action.addTodo(title,description,year,month,date,hour,minute));
    }
})

export default connect(mapStateToProps,mapDispatchToProps)(HomeScreen);